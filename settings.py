#!/usr/bin/env python

# DO NOT INCLUDE IN OFFICIAL VERSIONS.
# This file is here is ment for experimenting with yaml files.

import yaml

with open("settings.yaml", 'r') as stream:
    settings = yaml.load(stream)
    #print(settings)

for server in settings:
    if server == "globals":
        break
    print(server)
    port = settings[server]["port"]
    print(port)
    for name in settings[server]["names"]:
        print(name)
    name = settings[server]["names"][0]
    print("First name in list: " + name)

    """
    # Add if no commands do nothin
    for command in settings[0][server]["command"]:
        print(command)
        print("\n")
    """

    for channel in settings[server]["channels"]:
        print(channel)
        # fix if none.
        for bot in settings[server]["channels"][channel]["bots"]:
            print(bot)
        # fix if none
        for show in settings[server]["channels"][channel]["shows"]:
            print(show)
